// Utility to access google calendar.
// Ken Miyashita
//
// See https://developers.google.com/calendar/quickstart/js

const CLIENT_ID = '69902209462-ncb3s9kvd1u4pbj9dg145ku6lked05e3.apps.googleusercontent.com';
const API_KEY = 'AIzaSyA-Ipe-m8LNk9z98A0w8GWy9o6lqj4YrSY';
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
const SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

let isInitialized = false;

/**
  * Initialize google calendar API accessor.
  * You should call this function only once when your react Component
  * is mounted.
  *
  * @return {Promise}  Promise object. 'then()' function is called when API
  *                    becomes ready.
  *                    'catch()' function is called when already initialized.
  */
export const initialize = () => {
  // load and initialize google API.
  return new Promise((resolve, reject) => {
    if (! isInitialized) {
      isInitialized = true;
      window.gapi.load('client:auth2', resolve);
    } else {
      reject();
    }
  })
  .then(() => {
    return window.gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPES
    });
  });
}

/**
  * Check if the user has signed in or not.
  * @return {bool}  true when signed in.
  */
export const isSignedIn = () => {
  return window.gapi.auth2.getAuthInstance().isSignedIn.get();
}

/**
  * Sign in the google calendar.
  * @return {Promise}  Promise object. 'then()' function is called when
  *                    succeed in signing in, or already has signed in.
  *                    'then()' is defined as follows:
  *                    func(isSignedIn: bool)
  */
export const signIn = () => {
  return new Promise((resolve, reject) => {
    if (! isSignedIn()) {
      // will sign in.
      window.gapi.auth2.getAuthInstance().isSignedIn.listen(resolve);
      window.gapi.auth2.getAuthInstance().signIn();
    } else {
      // already signed in.
      resolve(true);
    }
  });
}

  /**
    * Sign out of the google calendar.
    * @return {Promise}  Promise object. 'then()' function is called when
    *                    succeed in signing out, or already has signed out.
    *                    'then()' is defined as follows:
    *                    func(isSignedIn: bool)
    */
export const signOut = () => {
  return new Promise((resolve, reject) => {
    if (isSignedIn()) {
      // will sign out.
      window.gapi.auth2.getAuthInstance().isSignedIn.listen(resolve);
      window.gapi.auth2.getAuthInstance().signOut();
    } else {
      // already signed out.
      resolve(false);
    }
  });
}

/**
  * List the summary and start datetime/date of the month.
  *
  * @param {int} year e.g. 2018
  * @param {int} month (0: Jan, 1: Feb, ...)
  * @return {Promise} Promise object. 'then()' is called when events are
  *                   retrieved from the google server.
  *                   'then()' is defined as follows:
  *                   func(eventArray: array_of_events)
  *                   eventArray[0] contains an empty array.
  *                   eventArray[1] contains an array of events for the 1st day.
  *                   eventArray[2] contains an array of events for the 2nd day, and so on.
  */
export function listMonthEvents(year, month){
  const startDate = (new Date(year, month)).toISOString();
  let endDate;
  if(month < 11){
    endDate = (new Date(year, month+1)).toISOString();
  }else{
    endDate = (new Date(year+1, 0)).toISOString();
  }

  return (window.gapi.client.calendar.events.list({
    'calendarId': 'primary',
    'timeMin': startDate,
    'timeMax': endDate,
    'showDeleted': false,
    'singleEvents': true,
    'orderBy': 'startTime'
  }).then((response) => {
    // classify events according to their start time.
    const eventArray = new Array(32);
    for(let i = 0; i < 32; i++){
      eventArray[i] = [];
    }
    const events = response.result.items;
    for(let i = 0; i < events.length; i++){
      const e = events[i];
      let date = e.start.dateTime;  // e.g.) 2018-03-15T00:00:00
      if(! date){
        date = e.start.date;        // e.g.) 2018-03-15
      }
      const dayNum = parseInt(date.substring(8, 10), 10);   // e.g.) 15
      eventArray[dayNum].push(e);
    }
    // notify the caller of the result.
    return new Promise((resolve) => {
      resolve(eventArray);
    });
  }));
}
