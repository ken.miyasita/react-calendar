import React from 'react';
import Navi     from '../containers/Navi';
import Calendar from '../containers/Calendar';
import Schedule from '../containers/Schedule';

const App = () => {
  return(
    <div className="app">
      <Navi/>
      <Calendar/>
      <Schedule/>
    </div>
  );
}

export default App;
