import React from 'react';

class Calendar extends React.Component {
  render() {
    return(
      <div>
        {this.renderMonth()}
      </div>
    );
  }

  /**
    * render a month.
    */
  renderMonth() {
    const year = this.props.navi.year;
    const month = this.props.navi.month;
    const dayOfWeek1st = (new Date(year, month, 1)).getDay();
    const numOfDays = this.calcNumOfDaysInMonth(year, month);

    return (
      <div>
        <div className="board-row">
          <div className='dayofweektile'>Sun</div>
          <div className='dayofweektile'>Mon</div>
          <div className='dayofweektile'>Tue</div>
          <div className='dayofweektile'>Wed</div>
          <div className='dayofweektile'>Thu</div>
          <div className='dayofweektile'>Fri</div>
          <div className='dayofweektile'>Sat</div>
        </div>
        <div>
          {this.renderWeek(0, dayOfWeek1st, numOfDays)}
          {this.renderWeek(1, dayOfWeek1st, numOfDays)}
          {this.renderWeek(2, dayOfWeek1st, numOfDays)}
          {this.renderWeek(3, dayOfWeek1st, numOfDays)}
          {this.renderWeek(4, dayOfWeek1st, numOfDays)}
          {this.renderWeek(5, dayOfWeek1st, numOfDays)}
        </div>
      </div>
    );
  }

  /**
    * render a row of a week.
    *
    * @param {int} weekIdx       the row index starting from 0
    * @param {int} dayOfWeek1st  what day of the week is the 1st day of
    *                            this month? (0:Sun, 1:Mon, 2:Tue ...)
    * @param {int} numOfDays     the number of days in this month.
    */
  renderWeek(weekIdx, dayOfWeek1st, numOfDays){
    var dateSun = weekIdx * 7 + 1 - dayOfWeek1st;
    var dateArray = []
    for(var i = 0; i < 7; i++){
      // which day of the month? 0 means invalid.
      var date = dateSun + i;
      if(date < 0){
        date = 0;
      }else if(date > numOfDays){
        date = 0;
      }
      // create the event indicator
      var eventNum = 0;
      if(this.props.navi.events.length > 0){
        eventNum = this.props.navi.events[date].length;
      }
      var eventIndicator;
      if(date === 0){
        eventIndicator = '';
      }else if(eventNum === 0){
        eventIndicator = '-';
      }else if(eventNum === 1){
        eventIndicator = '*';
      }else if(eventNum === 2){
        eventIndicator = '**';
      }else{
        eventIndicator = '***';
      }

      var selected = ((date !== 0) &&
                      (date === this.props.navi.dayOfMonth))
                      ? true : false;
      dateArray.push(<DateTile key={i} date={date} selected={selected}
                                eventIndicator={eventIndicator}
                                onClick={this.props.selectDate}/>);
    }
    return (
      <div className="board-row">
        {dateArray}
      </div>
    );
  }

  /**
    * Calculate the number of days in a certain month.
    *
    * @param {int} year   e.g) 2018
    * @param {int} month  0: Jan, 1: Feb, ...
    *
    * @return {int} numOfDays
    */
  calcNumOfDaysInMonth(year, month){
    if(month === 3 || month === 5 || month === 8 || month === 10){
      return 30;
    }else if(month === 2){
      if(year % 4 === 0){
        // leap year
        return 29;
      }else{
        return 28;
      }
    }else{
      return 31;
    }
  }
}

/**
  * @param {int} date  the day of the month (1-31) or 0 when empty.
  * @param {bool} selected  true if this tile is selected
  * @param {string} eventIndicator   string showing events e.g.) '***'
  * @param {function(int)} onClick   callback to call when a DateTile is clicked.
  */
const DateTile = (props) => {
  var className = (props.selected) ? 'datetile-selected' : 'datetile';
  return (
    <button className={className} onClick={()=>{props.onClick(props.date)}}>
      {(props.date === 0) ? '' : '' + props.date}
    <br/>
      {props.eventIndicator}
    </button>
  );
}

export default Calendar;
