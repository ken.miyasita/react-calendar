import React from 'react';

const Schedule = (props) => {
  return(
    <div className="day-info">
      <div>Schedule of the Day</div>
      <ol>
        {props.navi.events.length === 0 ?
        <EventList events={[]}/> :
        <EventList events={props.navi.events[props.navi.dayOfMonth]}/>}
      </ol>
    </div>
  );
}

/**
  * @param {Array} events Array of events.
  */
const EventList = (props) => {
  var liArray = [];
  for(var i = 0; i < props.events.length; i++){
    var e = props.events[i];
    var time = '--:--'
    if(e.start.dateTime){     // e.g.) 2018-03-15T00:00:00
      time = e.start.dateTime.substring(11,16);
    }
    liArray.push(<li key={[e.etag, time]}>
      {'[' + time + '] ' + e.summary}</li>);
  }
  return liArray;
}


export default Schedule;
