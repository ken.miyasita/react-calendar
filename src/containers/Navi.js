import { connect } from 'react-redux'
import Navi from '../components/Navi'
import * as module from '../modules/navi'
import * as flowSample from '../modules/flow-sample'

const mapStateToProps = (state, ownProps) => {
  return state;
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initGoogleApi: () => {dispatch(module.initGoogleApi());},
    signIn: () => {dispatch(module.signIn());},
    signOut: () => {dispatch(module.signOut());},
    prevMonth: () => {dispatch(module.prevMonth());},
    nextMonth: () => {dispatch(module.nextMonth());},
    testFlow: flowSample.testFlow
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Navi);
